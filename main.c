#include <CoOS.h>
#include "stm32f4xx_conf.h"
#include "BSP.h"
#include "LedTask.h"
#include "JoyTmr.h"
#include "LCDTask.h"


void SystemInit(void);

void CreateSystemObjetcs(void){
	//Inicialización de los elementos compartidos: flags, semaf., colas...
	CreateJoyFlags();		//Crear banderas del joystick
}

int Sumatory(int n){ç
int i,j=0;
    for(i=0;i<n;i++){
        j=j+i;
    }
    return j;
}
void CreateUserTasks(void){
	//Creación de las tareas de usuario
	CreateLedTask();
	CreateJoyTask();
	CreateLCDTask();
	Sumatory(20);
}


int main(void)
{

	SystemInit();			//Inicialización del reloj

	CoInitOS ();			//Inicialización del CoOs

	CreateSystemObjetcs();	//Inicialización Sem, flags, queues...

	CreateUserTasks();		//Creación de Tareas

	CoStartOS ();			//Comienzo de ejecución del planificador

    while(1)				//La ejecución nunca debería llegar aquí
    {
    }
}

